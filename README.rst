=====================
CKEditor static files
=====================

A simple Django app that contains the static files for CKEditor (http://ckeditor.com)


Quick start
-----------

1. Install with pip::

    pip install django-ckeditor-staticfiles

2. Add "ckeditor_staticfiles" to your INSTALLED_APPS setting::

    INSTALLED_APPS = (
        ...
        'ckeditor_staticfiles',
        ...
    )
